class AddUniqIndexToUrl < ActiveRecord::Migration[5.1]
  def change
    add_index :web_pages, :url, unique: true
  end
end
