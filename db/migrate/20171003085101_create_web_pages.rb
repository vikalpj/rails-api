class CreateWebPages < ActiveRecord::Migration[5.1]
  def change
    create_table :web_pages do |t|
      t.string :url
      t.string :content

      t.timestamps
    end
  end
end
