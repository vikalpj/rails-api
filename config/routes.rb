Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get :urls, to: 'url_content#index'
  post :urls, to: 'url_content#create'
end
