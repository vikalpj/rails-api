require 'open-uri'

class FetchUrlContentJob < ApplicationJob
  queue_as :default

  def perform(web_page)
    content = parse_page web_page.url

    # save webpage content
    web_page.content = content
    web_page.save
  end

  private
    ELEMENTS_TO_PARSE = ["h1", "h2", "a"]

    def parse_page(url)
      @content = ''
      begin
        document = Nokogiri::HTML(open(url))
      rescue StandardError
        return ''
      end

      # Traverse all nodes and check for the required nodes
      document.traverse do |node|
        next unless node.is_a?(Nokogiri::XML::Element)

        if ELEMENTS_TO_PARSE.include? node.name
          @content += "#{node.content} "
        end
      end

      @content
    end
end
