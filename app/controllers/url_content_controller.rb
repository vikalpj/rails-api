class UrlContentController < ApplicationController
  def index
    @web_pages = WebPage.all
    render json: @web_pages
  end

  def create
    @createParams = Hash.new
    @createParams[:url] = params[:url]

    @new_object = WebPage.new @createParams

    if @new_object.valid?
      @new_object.save!
      render json: @new_object, status: :accepted
    else
      render json: @new_object.errors, status: :unprocessable_entity
    end
  end
end

