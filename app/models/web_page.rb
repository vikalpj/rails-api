class WebPage < ApplicationRecord
  after_create :fetch_page

  validates :url, presence: true, uniqueness: true
  validate :validate_url

  private
    def fetch_page
      FetchUrlContentJob.perform_later self
    end

    def validate_url
      if not self.url =~ URI::regexp
        errors.add(:url, 'Url is not correct. Should be in format http(s)://subdomain.domain.tld')
      end
    end
end
