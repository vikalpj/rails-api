[TOC]

# README Rails Rampup

## Install rvm

```
https://rvm.io/rvm/install
```

## Clone the repo and go to directory with code 

```
git clone git@bitbucket.org:vikalpj/rails-api.git && cd rails-api
```

## Install Bundler

```
gem install bundler
```

## Install other gems

```
bundle install
```

## Migrate DB
__NOTE__ Sql lite 3 is used as of now

```
bin/rails db:migrate
```

## Run server 

```
bin/rails server
```

## Run jobs 

__NOTE__ Jobs are needed because Urls are parsed over async tasks.

```
bundle exec sidekiq -q default -q mailers
```

__NOTE__

1. Postman collection is included in the directory postman_collection